﻿using UnityEngine;
using System;

public class CollectPowerUp : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        // Player collected the power-up. We need to activate it.
        // Get the name of the main script from the game object name.
        string scriptClassName = gameObject.name;
        IPowerUp powerUpScript = other.gameObject.AddComponent(Type.GetType(scriptClassName)) as IPowerUp;
        // Initialize the power-up script.
        powerUpScript.Initialize();

        // We don't need a power-up game object anymore so destroy it.
        Destroy(gameObject);
    }
}
