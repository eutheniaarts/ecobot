﻿using UnityEngine;

public class LevelExit : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        // Currently, this quits the game. But in future, this needs to load a next level.
        Application.Quit();
    }
}
