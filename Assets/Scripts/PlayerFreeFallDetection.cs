﻿using UnityEngine;

public class PlayerFreeFallDetection : MonoBehaviour
{
    public float freeFallMargin = 2f;
    private Rigidbody playerRigidbody;
    bool isVelocityChanged;

    void Start()
    {
        playerRigidbody = gameObject.GetComponent<Rigidbody>();
        isVelocityChanged = false;
    }
	
    void LateUpdate()
    {
        Vector3 localVelocity = transform.parent.InverseTransformDirection(playerRigidbody.velocity);

        if (isVelocityChanged)
        {
            if (Mathf.RoundToInt(localVelocity.y) == 0)
            {
                // Player landed on a platform and can now freely move.
                EventManager.TriggerEvent(new PlayerStoppedFalling());
                //playerMovementScript.EnableMovement();
                isVelocityChanged = false;
            }
            else
            {
                // The player is falling. Disable the movement.
                EventManager.TriggerEvent(new PlayerStartedFalling());
                //playerMovementScript.DisableMovement();
            }
        }

        if (localVelocity.y < -freeFallMargin)
        {
            // The player is falling. Disable the movement.
            EventManager.TriggerEvent(new PlayerStartedFalling());
            //playerMovementScript.DisableMovement();
            isVelocityChanged = true;
        }
    }
}
