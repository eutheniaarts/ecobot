using UnityEngine;

class PlayerStepedOnCubeEvent : GameEvent
{
    public GameObject cubeGameObject { get; private set; }

    public PlayerStepedOnCubeEvent(GameObject cubeGameObject)
    {
        this.cubeGameObject = cubeGameObject;
    }
}

