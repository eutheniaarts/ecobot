﻿using UnityEngine;

internal class CubeDestroyedEvent : GameEvent
{
    public GameObject cubeGameObject { get; private set; }

    public CubeDestroyedEvent(GameObject cubeGameObject)
    {
        this.cubeGameObject = cubeGameObject;
    }
}