﻿class PlayerInputMouseRightButtonEvent : GameEvent
{
    public float mouseX { get; private set; }
    public float mouseY { get; private set; }

    public PlayerInputMouseRightButtonEvent(float mouseX, float mouseY)
    {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }
}