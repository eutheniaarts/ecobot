using UnityEngine;

class PlayerStepedOffCubeEvent : GameEvent
{
    public GameObject cubeGameObject { get; private set; }
    
    public PlayerStepedOffCubeEvent(GameObject cubeGameObject)
    {
        this.cubeGameObject = cubeGameObject;
    }
}

