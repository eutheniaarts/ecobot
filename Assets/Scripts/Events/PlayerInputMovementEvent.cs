﻿class PlayerInputMovementEvent : GameEvent
{
    public float horizontalAxis { get; private set; }
    public float verticalAxis { get; private set; }

    public PlayerInputMovementEvent(float horizontalAxis, float verticalAxis)
    {
        this.horizontalAxis = horizontalAxis;
        this.verticalAxis = verticalAxis;
    }
}