﻿using UnityEngine;

public class LevelStart : MonoBehaviour {
    private static string playerPrefabName = "Player";
    private static string playerPrefabPath = "Prefabs/" + playerPrefabName;

    void Start() {
        // Create the player and put him on a start position.
        GameObject playerObject = Instantiate(Resources.Load(playerPrefabPath)) as GameObject;
        playerObject.name = playerPrefabName;
        playerObject.transform.localPosition = new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z);
    }
}
