﻿using System;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    public float distance = 10.0f;
    public float xSpeed = 250.0f;
    public float ySpeed = 250.0f;
    public float yMinLimit = -40;
    public float yMaxLimit = 60;
    public float smoothTime = 0.3f;
    private Transform playerCharacterTransform;
    private float x = 0.0f;
    private float y = 0.0f;
    private float xSmooth = 0.0f;
    private float ySmooth = 0.0f;
    private float xVelocity = 0.0f;
    private float yVelocity = 0.0f;
    private Vector3 posSmooth = Vector3.zero;
    private float mouseX;
    private float mouseY;
    private bool hasRotated;

    void Start()
    {
        playerCharacterTransform = LevelManager.Instance.GetPlayerCharacter().transform;

        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        RegisterGameEventListener();
    }

    void RegisterGameEventListener()
    {
        EventManager.AddListener<PlayerInputMouseRightButtonEvent>(OnPlayerInputMouseRightButton);
    }

    private void OnPlayerInputMouseRightButton(PlayerInputMouseRightButtonEvent gameEvent)
    {
        mouseX = gameEvent.mouseX;
        mouseY = gameEvent.mouseY;

        hasRotated = true;
    }

    void LateUpdate()
    {
        if (hasRotated)
        {
            x += mouseX * xSpeed * 0.02f;
            y -= mouseY * ySpeed * 0.02f;
        }

        xSmooth = Mathf.SmoothDamp(xSmooth, x, ref xVelocity, smoothTime);
        ySmooth = Mathf.SmoothDamp(ySmooth, y, ref yVelocity, smoothTime);

        ySmooth = ClampAngle(ySmooth, yMinLimit, yMaxLimit);

        var rotation = Quaternion.Euler(ySmooth, xSmooth, 0);

        posSmooth = playerCharacterTransform.localPosition;

        transform.localRotation = rotation;
        transform.localPosition = rotation * new Vector3(0.0f, 0.0f, -distance) + posSmooth;

        hasRotated = false;
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
        {
            angle += 360;
        }
        if (angle > 360)
        {
            angle -= 360;
        }
        
        return Mathf.Clamp(angle, min, max);
    }
}
