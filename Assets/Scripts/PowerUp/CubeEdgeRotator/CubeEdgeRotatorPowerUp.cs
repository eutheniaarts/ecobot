using System;
using UnityEngine;

public class CubeEdgeRotatorPowerUp : MonoBehaviour, IPowerUp
{
    private CubeGapDetection cubeGapDetectionScript;
    private PlayerRotator levelRotatorScript;
    private GameObject playerBase;
    private GameObject playerGapDetectionPoint;
    private GameObject playerLevelRotationCollider;
    private bool powerUpEnabled;

    public void Start()
    {
        RegisterGameEventListener();
    }

    private void RegisterGameEventListener()
    {
        EventManager.AddListener<PlayerInputPowerUpActivationEvent>(OnPlayerInputPowerUpActivation);
    }

    private void OnPlayerInputPowerUpActivation(PlayerInputPowerUpActivationEvent gameEvent)
    {
        if (IsEnabled())
        {
            Disable();
        }
        else
        {
            Enable();
        }
    }

    public void Initialize()
    {
        playerGapDetectionPoint = LevelManager.Instance.GetPlayerGapDetectionPoint();
        playerBase = LevelManager.Instance.GetPlayerBase();
        playerLevelRotationCollider = LevelManager.Instance.GetPlayerLevelRotationCollider();

        cubeGapDetectionScript = playerGapDetectionPoint.AddComponent<CubeGapDetection>();
        levelRotatorScript = playerLevelRotationCollider.AddComponent<PlayerRotator>();
        playerLevelRotationCollider.AddComponent<GravityShifter>();
        playerBase.AddComponent<CubeFaceColliderPlacement>();
        powerUpEnabled = true;
    }

    public void Enable()
    {
        powerUpEnabled = true;
        cubeGapDetectionScript.enabled = true;
        levelRotatorScript.enabled = true;
    }

    public void Disable()
    {
        powerUpEnabled = false;
        cubeGapDetectionScript.enabled = false;
        levelRotatorScript.enabled = false;
        // In order for power-up to function normally CubeFaceColliderPlacement script must not be disabled!
        // The reason for this is that the script must place edge colliders even when the power-up itself is disabled.
    }

    private bool IsEnabled()
    {
        return powerUpEnabled;
    }
}
