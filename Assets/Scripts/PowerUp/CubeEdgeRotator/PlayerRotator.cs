﻿using UnityEngine;
using System.Collections;

public class PlayerRotator : MonoBehaviour
{
    public float rotateSpeed = 200.0f;

    private bool isInRotation;
    private CubeGapDetection cubeGapDetectionScript;
    private Vector3 rotationAxis;
    private GameObject playerGapDetectionPoint;
    private float currentAngle = 0.0f;
    private float previousAngle = 0.0f;
    private GameObject player;
    private Transform playerParent;

    void OnEnable()
    {
        enabled = true;
    }

    void OnDisable()
    {
        enabled = false;
    }

    void Awake()
    {
        player = LevelManager.Instance.GetPlayerCharacter();
        playerParent = player.transform.parent;
        playerGapDetectionPoint = LevelManager.Instance.GetPlayerGapDetectionPoint();
        cubeGapDetectionScript = playerGapDetectionPoint.GetComponent<CubeGapDetection>();
        isInRotation = false;
    }

    void OnTriggerExit(Collider other)
    {
        // This method will be called when LevelRotationCollider stops touching CubeFace.
        // At that moment the level should rotate around the player.
        if (other.tag == "CubeFace")
        {
            RotateAroundCubeEdge(other);
        }
    }

    void RotateAroundCubeEdge(Collider other)
    {
        if (enabled == false)
        {
            // Player has disabled level rotator power-up.
            // So we don't need to process any further until player enables power-up again.
            return;
        }
        if (isInRotation)
        {
            return;
        }
        if (cubeGapDetectionScript.IsInFrontOfGap())
        {
            rotationAxis = CalculateRotationAxisAndDirection(other);
            // Notify that the player will rotate.
            EventManager.TriggerEvent(new PlayerWillRotateEvent());
            StartCoroutine(DoRotate());
        }
    }

    private IEnumerator DoRotate()
    {
        isInRotation = true;

        while (currentAngle != 90f)
        {
            currentAngle = Mathf.MoveTowardsAngle(currentAngle, 90.0f, rotateSpeed * Time.deltaTime);
            float deltaAngle = currentAngle - previousAngle;
            previousAngle = currentAngle;

            player.transform.parent.RotateAround(transform.position, rotationAxis, deltaAngle);

            yield return null;
        }

        // Rotation if completed. Reset the rotation state.
        currentAngle = 0.0f;
        previousAngle = 0.0f;
        isInRotation = false;
        
        // Notify the rest that the player has been rotated.
        EventManager.TriggerEvent(new PlayerRotatedEvent());
    }

    private Vector3 CalculateRotationAxisAndDirection(Collider cubeFaceCollider)
    {
        // We need to find the closest specific point on the CubeFace relative to the player position.
        Vector3 closestSpecificPoint = Vector3.zero;
        Vector3 rotationAxis = Vector3.zero;
        foreach (Transform pointOnCubeFace in cubeFaceCollider.transform)
        {
            float pointOnCubeFaceDistance = Vector3.Distance(pointOnCubeFace.position, transform.position);
            float closestSpecificPointDistance = Vector3.Distance(closestSpecificPoint, transform.position);

            if (pointOnCubeFaceDistance <= closestSpecificPointDistance)
            {
                Vector3 axis = CalculateAxisForPoint(pointOnCubeFace.position);
                if (axis == Vector3.zero)
                {
                    // We will ignore this point since it is not on the edge.
                    continue;
                }

                closestSpecificPoint = pointOnCubeFace.position;
                rotationAxis = axis;
            }
        }

        return rotationAxis;
    }

    private Vector3 CalculateAxisForPoint(Vector3 point)
    {
        // We need to determine the rotation axis and the rotation direction.
        // To determine this, we can use a raycast in four points relative to the point.
        Vector3 diff = playerParent.TransformDirection(new Vector3(0.5f, 0.5f, 0f));
        bool isRightHit = SphereCastAtPointWithDelta(point, diff);
        if (isRightHit == false)
        {
            // isRightHit determine the Z axis in negative direction.
            return -playerParent.forward;
        }

        diff = playerParent.TransformDirection(new Vector3(-0.5f, 0.5f, 0f));
        bool isLeftHit = SphereCastAtPointWithDelta(point, diff);
        if (isLeftHit == false)
        {
            // isLeftHit determine the Z axis in positive direction.
            return playerParent.forward;
        }

        diff = playerParent.TransformDirection(new Vector3(0f, 0.5f, 0.5f));
        bool isUpHit = SphereCastAtPointWithDelta(point, diff);
        if (isUpHit == false)
        {
            // isUpHit determine the X axis in positive direction.
            return playerParent.right;
        }

        diff = playerParent.TransformDirection(new Vector3(0f, 0.5f, -0.5f));
        bool isDownHit = SphereCastAtPointWithDelta(point, diff);
        if (isDownHit == false)
        {
            // isDownHit determine the X axis in negative direction.
            return -playerParent.right;
        }

        // All sphere casts reported that they hit a floor. This means that the specific point is not on the edge.
        // In this case we will return Vector3.zero to indicate this state.
        return Vector3.zero;
    }

    private bool SphereCastAtPointWithDelta(Vector3 point, Vector3 delta)
    {
        Ray rightRay = new Ray(point + delta, -player.transform.up);
        return Physics.SphereCast(rightRay, 0.1f, 0.5f);
    }
}