﻿using UnityEngine;

public class CubeGapDetection : MonoBehaviour
{
    private GameObject player;

    public void Start()
    {
        player = LevelManager.Instance.GetPlayerCharacter();
    }

    public bool IsInFrontOfGap()
    {
        // Since this method is called after the player exits from the cubeFace collider but still standing on the cube
        // we can use a raycast to determine if the player in front of a gap.        
        bool isHit = Physics.Raycast(transform.position, -player.transform.up, 0.5f);

        return !isHit;
    }
}
