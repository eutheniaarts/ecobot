﻿using UnityEngine;

public class GravityShifter : MonoBehaviour
{
    private GameObject player;

    public void Start()
    {
        player = LevelManager.Instance.GetPlayerCharacter();
        RegisterGameEventListener();
    }

    private void RegisterGameEventListener()
    {
        EventManager.AddListener<PlayerWillRotateEvent>(OnLevelWillRotateEvent);
        EventManager.AddListener<PlayerRotatedEvent>(OnLevelRotatedEvent);
    }

    private void OnLevelWillRotateEvent(PlayerWillRotateEvent gameEvent)
    {
        GravityManager.Instance.SetGravity(Vector3.zero);
    }

    private void OnLevelRotatedEvent(PlayerRotatedEvent gameEvent)
    {
        GravityManager.Instance.SetGravity(-player.transform.up);
    }
}
