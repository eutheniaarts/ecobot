﻿using UnityEngine;
using System.Collections;
using System;

public class CubeFaceColliderPlacement : MonoBehaviour
{
    private static string cubeFacePrefabName = "CubeFace";
    private static string cubeFacePrefabPath = "Prefabs/Miscellaneous/" + cubeFacePrefabName;
    private ArrayList cubeFaces;
    private ArrayList activeCubeFaces;
    private GameObject level;
    private GameObject player;

    void Start()
    {
        level = LevelManager.Instance.GetLevel();
        player = LevelManager.Instance.GetPlayerCharacter();
        RegisterGameEventListener();
        CreateFaceColliders();
        PutInitialEdgeColliders();
        activeCubeFaces = new ArrayList();
    }

    private void PutInitialEdgeColliders()
    {
        foreach (GameObject currentStandingCube in CubeTouchDetection.GetCurrentStandingCubes())
        {
            PositionFaceColliders(currentStandingCube.transform);
        }
    }

    private void CreateFaceColliders()
    {
        // Instantiate all CubeFace prefabs.
        cubeFaces = new ArrayList();

        for (int i = 0; i < 4; i++)
        {
            GameObject cubeFaceObject = Instantiate(Resources.Load(cubeFacePrefabPath)) as GameObject;
            cubeFaceObject.name = cubeFacePrefabName;
            cubeFaceObject.transform.parent = level.transform;
            cubeFaceObject.SetActive(false);

            cubeFaces.Add(cubeFaceObject);
        }
    }

    void RegisterGameEventListener()
    {
        EventManager.AddListener<PlayerStepedOnCubeEvent>(OnPlayerStepedOnCube);
        EventManager.AddListener<PlayerStepedOffCubeEvent>(OnPlayerStepedOffCube);
        EventManager.AddListener<CubeDestroyedEvent>(OnCubeDestroyed);
        EventManager.AddListener<PlayerWillRotateEvent>(OnLevelWillRotateEvent);
        EventManager.AddListener<PlayerRotatedEvent>(OnLevelRotatedEvent);
    }

    void OnPlayerStepedOnCube(PlayerStepedOnCubeEvent gameEvent)
    {
        // We need to check if the Cube already has an active cubeFace on it.
        foreach(Transform childTransform in gameEvent.cubeGameObject.transform)
        {
            if (childTransform.tag == "CubeFace")
            {
                if (childTransform.gameObject.activeSelf)
                {
                    // No need to put additional cubeFace object on this cube since it already have an active cubeFace.
                    return;
                }
            }
        }

        PositionFaceColliders(gameEvent.cubeGameObject.transform);
    }

    void OnPlayerStepedOffCube(PlayerStepedOffCubeEvent gameEvent)
    {
        // Player has left the previously steped cube.
        // We need to disable all cube edge colliders that we created on that cube.
        foreach (Transform child in gameEvent.cubeGameObject.transform)
        {
            if (child.name.Equals(cubeFacePrefabName))
            {
                int index = cubeFaces.IndexOf(child.gameObject);
                if (index == -1)
                {
                    continue;
                }

                GameObject cubeEdgesObject = cubeFaces[index] as GameObject;
                cubeEdgesObject.SetActive(false);
            }
        }
    }

    void OnCubeDestroyed(CubeDestroyedEvent gameEvent)
    {
        // A cube has been destroyed and we need to check if that cube had edge colliders attached.
        foreach (Transform child in gameEvent.cubeGameObject.transform)
        {
            if (child.name.Equals(cubeFacePrefabName))
            {
                // Cube indeed had edge colliders attached.
                // Since the cube is destroyed, we also need to disable those edge colliders.
                child.gameObject.SetActive(false);
                child.gameObject.transform.parent = level.transform;
            }
        }
    }

    private void OnLevelWillRotateEvent(PlayerWillRotateEvent gameEvent)
    {
        activeCubeFaces.Clear();
        // Deactivate all active face colliders.
        foreach (GameObject cubeFaceObject in cubeFaces)
        {
            if (cubeFaceObject.activeSelf)
            {
                cubeFaceObject.SetActive(false);
                activeCubeFaces.Add(cubeFaceObject);
            }
        }
    }

    private void OnLevelRotatedEvent(PlayerRotatedEvent gameEvent)
    {
        // Rotate all previously active face colliders and activate them.
        foreach (GameObject cubeFaceObject in activeCubeFaces)
        {
            SetRotationAndPosition(cubeFaceObject);
            cubeFaceObject.SetActive(true);
        }
    }

    private void SetRotationAndPosition(GameObject cubeFaceObject)
    {
        cubeFaceObject.transform.rotation = player.transform.parent.rotation;
        Vector3 deltaPosition = player.transform.TransformDirection(new Vector3(0f, 0.5f, 0f));
        cubeFaceObject.transform.position = cubeFaceObject.transform.parent.transform.position + deltaPosition;
    }

    void PositionFaceColliders(Transform cubeTransform)
    {
        // Create and put all colliders.
        foreach (GameObject cubeFaceObject in cubeFaces)
        {
            if (cubeFaceObject.activeSelf)
            {
                // Cube edges object is already active so we can not use it.
                continue;
            }

            // The first inactive cube edges object is found.
            cubeFaceObject.transform.parent = cubeTransform;
            SetRotationAndPosition(cubeFaceObject);
            cubeFaceObject.SetActive(true);

            break;
        }
    }
}
