﻿/// <summary>
/// Interface for the player power-up.
/// Power-up is a game object which can be picked up by player and it augumnets the player possibilities.
/// </summary>
interface IPowerUp
{
    void Initialize();
    void Enable();
    void Disable();
}
