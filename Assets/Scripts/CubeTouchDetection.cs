﻿using UnityEngine;
using System.Collections;
using System;

public class CubeTouchDetection : MonoBehaviour
{
    private static ArrayList currentStandingCubes;
    private Collider baseCollider;

    public void Start()
    {
        currentStandingCubes = new ArrayList();
        RegisterGameEventListener();
        baseCollider = GetComponent<SphereCollider>();
    }

    void RegisterGameEventListener()
    {
        EventManager.AddListener<CubeDestroyedEvent>(OnCubeDestroyed);
        EventManager.AddListener<PlayerWillRotateEvent>(OnLevelWillRotateEvent);
        EventManager.AddListener<PlayerRotatedEvent>(OnLevelRotatedEvent);
    }

    private void OnLevelWillRotateEvent(PlayerWillRotateEvent gameEvent)
    {
        // Turn off collision handling.
        baseCollider.enabled = false;
    }

    private void OnLevelRotatedEvent(PlayerRotatedEvent gameEvent)
    {
        // Turn on collision handling.
        baseCollider.enabled = true;
    }

    void OnTriggerEnter(Collider other)
    {
        GameObject cube;
        bool isFloor = IsCubeOnFloor(other.gameObject, out cube);

        if (isFloor)
        {
            // Player has steped on a cube. Generate a game event.
            currentStandingCubes.Add(cube);
            EventManager.TriggerEvent(new PlayerStepedOnCubeEvent(other.gameObject));
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (currentStandingCubes.Contains(other.gameObject))
        {
            // Player has steped off a cube. Generate a game event.
            currentStandingCubes.Remove(other.gameObject);
            EventManager.TriggerEvent(new PlayerStepedOffCubeEvent(other.gameObject));
        }
    }

    void OnCubeDestroyed(CubeDestroyedEvent gameEvent)
    {
        // A cube has been destroyed and we need to check if the player has been standing on that cube.
        if (currentStandingCubes.Contains(gameEvent.cubeGameObject))
        {
            currentStandingCubes.Remove(gameEvent.cubeGameObject);
            EventManager.TriggerEvent(new PlayerStepedOffCubeEvent(gameEvent.cubeGameObject));
        }
    }

    private bool IsCubeOnFloor(GameObject gameObject, out GameObject cube)
    {
        // Check if player collided with a cube.
        if (gameObject.tag != "Cube")
        {
            // The collider that we hit is not a cube. Ignore.
            cube = null;

            return false;
        }

        cube = gameObject;

        return true;
    }

    public static ArrayList GetCurrentStandingCubes()
    {
        return currentStandingCubes;
    }
}
