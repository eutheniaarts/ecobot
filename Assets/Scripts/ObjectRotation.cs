﻿using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    public float rotationSpeed;
    public float x;
    public float y;
    public float z;
    
    void Update()
    {
        transform.Rotate(new Vector3(x, y, z) * rotationSpeed * Time.deltaTime);
    }
}
