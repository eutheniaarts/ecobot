﻿using UnityEngine;
using System.Collections;
using System;

public class BreakableCube : MonoBehaviour
{
    public float breakSpeed = 0f;
    private float timeOnCube = 0f;
    private IEnumerator breakCubeCoroutine;

    public void OnCollisionEnter(Collision collision)
    {
        // Check if the player is standing on cube.
        Vector3 collisionNormal = collision.contacts[0].normal;
        collisionNormal = collision.transform.parent.InverseTransformDirection(collisionNormal);
        if (isPlayerStandingOnCube(collisionNormal) == false)
        {
            return;
        }
        // Player has stepped on the breakable cube.
        // Start to measure time spent on cube.
        breakCubeCoroutine = BreakCube();
        StartCoroutine(breakCubeCoroutine);
    }

    private bool isPlayerStandingOnCube(Vector3 normal)
    {
        float gravityDirection = Math.Abs(normal.y);
        if (Mathf.RoundToInt(gravityDirection) == 1)
        {
            // Player is standing on cube.
            return true;
        } else
        {
            // Player is not standing on cube.
            return false;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        // Player has stepped off the breakable cube.
        // Stop the time measurement.
        if (breakCubeCoroutine == null)
        {
            return;
        }
        StopCoroutine(breakCubeCoroutine);
        breakCubeCoroutine = null;
    }

    private IEnumerator BreakCube()
    {
        while (timeOnCube < breakSpeed)
        {
            // Player is not standing long enough on a cube.
            // Wait a bit more.
            timeOnCube += Time.deltaTime;
            yield return null;
        }

        // Break the cube.
        EventManager.TriggerEvent(new CubeDestroyedEvent(gameObject));
        Destroy(gameObject);
    }
}
