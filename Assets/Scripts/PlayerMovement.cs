﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody playerCharacterRigidBody;
    private float moveHorizontal;
    private float moveVertical;
    private bool hasMoved;

    void Start()
    {
        playerCharacterRigidBody = GetComponent<Rigidbody>();
        playerCharacterRigidBody.freezeRotation = true;

        RegisterGameEventListener();
    }

    private void RegisterGameEventListener()
    {
        EventManager.AddListener<PlayerInputMovementEvent>(OnPlayerInputMovement);
        EventManager.AddListener<PlayerWillRotateEvent>(OnLevelWillRotateEvent);
        EventManager.AddListener<PlayerRotatedEvent>(OnLevelRotatedEvent);
        EventManager.AddListener<PlayerStartedFalling>(OnPlayerStartedFallingEvent);
        EventManager.AddListener<PlayerStoppedFalling>(OnPlayerStoppedFallingEvent);
    }

    private void OnPlayerStartedFallingEvent(PlayerStartedFalling gameEvent)
    {
        // We do not want a player movement during falling.
        DisableMovement();
    }

    private void OnPlayerStoppedFallingEvent(PlayerStoppedFalling gameEvent)
    {
        // Player has landed on platform and can now safely move.
        EnableMovement();
    }

    private void OnLevelWillRotateEvent(PlayerWillRotateEvent gameEvent)
    {
        // We do not want a player movement during a player rotation.
        DisableMovement();
    }

    private void OnLevelRotatedEvent(PlayerRotatedEvent gameEvent)
    {
        // Player has rotated and can now safely move.
        EnableMovement();
    }

    private void OnPlayerInputMovement(PlayerInputMovementEvent gameEvent)
    {
        moveVertical = gameEvent.verticalAxis;
        moveHorizontal = gameEvent.horizontalAxis;
        hasMoved = true;
    }

    void FixedUpdate()
    {
        if (hasMoved == false)
        {
            return;
        }

        Transform mainCamera = Camera.main.transform;
        Vector3 cameraUp = mainCamera.up;
        Vector3 cameraRight = mainCamera.right;

        Vector3 movement = cameraUp * moveVertical + cameraRight * moveHorizontal;
        
        movement = transform.InverseTransformDirection(movement);
        movement.y = 0;
        movement = transform.TransformDirection(movement);
        movement = Vector3.ClampMagnitude(movement, 1f);
        playerCharacterRigidBody.MovePosition(transform.position + movement * moveSpeed * Time.deltaTime);
        transform.LookAt(transform.position + movement, transform.up);

        hasMoved = false;
    }

    public void DisableMovement()
    {
        EventManager.RemoveListener<PlayerInputMovementEvent>(OnPlayerInputMovement);
    }

    public void EnableMovement()
    {
        EventManager.AddListener<PlayerInputMovementEvent>(OnPlayerInputMovement);
    }
}
