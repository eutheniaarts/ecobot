﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private GameObject level;
    private GameObject playerBase;
    private GameObject playerGapDetectionPoint;
    private GameObject playerRotationCollider;
    private GameObject playerCharacter;

    public static LevelManager Instance { get; private set; }

    void Start()
    {
        Instance = this;

        level = GameObject.Find("Level");
        playerGapDetectionPoint = GameObject.Find("GapDetectionPoint");
        playerBase = GameObject.Find("Base");
        playerRotationCollider = GameObject.Find("RotationCollider");
        playerCharacter = GameObject.FindGameObjectWithTag("PlayerCharacter");
    }

    public GameObject GetLevel()
    {
        return level;
    }

    public GameObject GetPlayerBase()
    {
        return playerBase;
    }

    public GameObject GetPlayerGapDetectionPoint()
    {
        return playerGapDetectionPoint;
    }

    public GameObject GetPlayerLevelRotationCollider()
    {
        return playerRotationCollider;
    }

    public GameObject GetPlayerCharacter()
    {
        return playerCharacter;
    }
}
