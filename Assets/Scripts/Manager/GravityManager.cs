﻿using UnityEngine;

public class GravityManager : MonoBehaviour
{
    public static GravityManager Instance { get; private set; }
    private float gravityValue = 9.8f;

    void Start ()
    {
        Instance = this;
        // Set gravity to normal.
        SetGravity(Vector3.down);
	}

    public void SetGravity(Vector3 gravity)
    {
        Physics.gravity = gravity * gravityValue;
    }
}
