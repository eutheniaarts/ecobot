﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInputManager : MonoBehaviour {
    // According to the http://docs.unity3d.com/ScriptReference/Input.html, it is suggested
    // that all of calls to the Input class are done in the Update() method.
    // This is because the input flags are reset on every Update() call.
    void Update () {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");
        if (horizontalAxis != 0f || verticalAxis != 0f)
        {
            EventManager.TriggerEvent(new PlayerInputMovementEvent(horizontalAxis, verticalAxis));
        }

        if (Input.GetMouseButton(1))
        {
            // Player has pressed right mouse button.
            // Get the mouse cursor coordinates.
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            EventManager.TriggerEvent(new PlayerInputMouseRightButtonEvent(mouseX, mouseY));
        }

        if (Input.GetButtonUp("Activate/Deactivate Power-Up"))
        {
            EventManager.TriggerEvent(new PlayerInputPowerUpActivationEvent());
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            EventManager.RemoveAll();
            Scene loadedScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(loadedScene.name);
        }
    }
}
