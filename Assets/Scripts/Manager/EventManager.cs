﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEvent {}

public static class EventManager {
    public delegate void EventDelegate<T> (T e) where T : GameEvent;
    private delegate void EventDelegate(GameEvent e);

    private static Dictionary<System.Type, EventDelegate> delegates = new Dictionary<System.Type, EventDelegate>();
    private static Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();
    private static Dictionary<System.Delegate, System.Delegate> onceLookups = new Dictionary<System.Delegate, System.Delegate>();

    private static EventDelegate AddDelegate<T>(EventDelegate<T> del) where T : GameEvent {
        // Early-out if we've already registered this delegate
        if (delegateLookup.ContainsKey(del))
            return null;

        // Create a new non-generic delegate which calls our generic one.
        // This is the delegate we actually invoke.
        EventDelegate internalDelegate = (e) => del((T)e);
        delegateLookup[del] = internalDelegate;

        EventDelegate tempDel;
        if (delegates.TryGetValue(typeof(T), out tempDel)) {
            delegates[typeof(T)] = tempDel += internalDelegate; 
        } else {
            delegates[typeof(T)] = internalDelegate;
        }

        return internalDelegate;
    }

    public static void AddListener<T> (EventDelegate<T> del) where T : GameEvent {
        AddDelegate<T>(del);
    }

    public static void AddListenerOnce<T> (EventDelegate<T> del) where T : GameEvent {
        EventDelegate result = AddDelegate<T>(del);

        if(result != null){
            // Remember that listener should be only called once.
            onceLookups[result] = del;
        }
    }

    public static void RemoveListener<T> (EventDelegate<T> del) where T : GameEvent {
        EventDelegate internalDelegate;
        if (delegateLookup.TryGetValue(del, out internalDelegate)) {
            EventDelegate tempDel;
            if (delegates.TryGetValue(typeof(T), out tempDel)){
                tempDel -= internalDelegate;
                if (tempDel == null){
                    delegates.Remove(typeof(T));
                } else {
                    delegates[typeof(T)] = tempDel;
                }
            }

            delegateLookup.Remove(del);
        }
    }

    public static void RemoveAll(){
        delegates.Clear();
        delegateLookup.Clear();
        onceLookups.Clear();
    }

    public static bool HasListener<T> (EventDelegate<T> del) where T : GameEvent {
        return delegateLookup.ContainsKey(del);
    }

    public static void TriggerEvent (GameEvent e) {
        EventDelegate del;
        if (delegates.TryGetValue(e.GetType(), out del)) {
            del.Invoke(e);

            // Remove listeners which should only be called once.
            foreach(EventDelegate k in delegates[e.GetType()].GetInvocationList()){
                if(onceLookups.ContainsKey(k)){
                    delegates[e.GetType()] -= k;

                    if(delegates[e.GetType()] == null)
                    {
                        delegates.Remove(e.GetType());
                    }

                    delegateLookup.Remove(onceLookups[k]);
                    onceLookups.Remove(k);
                }
            }
        }
    }
}