﻿using NUnit.Framework;
using NSubstitute;

public class EventManagerTest
{
    IGameEventHandler eventHandler;

    [SetUp]
    public void TestSetUp()
    {
        EventManager.RemoveAll();
        eventHandler = Substitute.For<IGameEventHandler>();
    }

    /// <summary>
    /// Tests whether the <c>EventManager</c> doesn't have a specific listener if this
    /// listener is not registered with the <c>EventManager</c>.
    /// </summary>
    [Test]
    public void HasNoListenerWhenNotAdded()
    {
        bool hasListener = EventManager.HasListener<GameEvent>(eventHandler.HandleGameEvent);

        // Assert that EventManager has no listener when there is no registered listener.
        Assert.IsFalse(hasListener);
    }

    /// <summary>
    /// Tests whether the <c>EventManager</c> has a specific listener if this
    /// listener is registered with the <c>EventManager</c>.
    /// </summary>
    [Test]
    public void HasListenerWhenAdded()
    {
        EventManager.AddListener<GameEvent>(eventHandler.HandleGameEvent);
        bool hasListener = EventManager.HasListener<GameEvent>(eventHandler.HandleGameEvent);

        // Assert that EventManager has a listener if it was previously registered to the EventManager.
        Assert.IsTrue(hasListener);
    }

    /// <summary>
    /// Tests whether the <c>EventManager</c> has no specific listener
    /// if it was previously removed from the <c>EventManager</c>.
    /// </summary>
    [Test]
    public void HasNoListenerWhenRemoved()
    {
        EventManager.AddListener<GameEvent>(eventHandler.HandleGameEvent);
        EventManager.RemoveListener<GameEvent>(eventHandler.HandleGameEvent);
        bool hasListener = EventManager.HasListener<GameEvent>(eventHandler.HandleGameEvent);

        // Assert that EventManager has no listener if it was previously removed from the EventManager.
        Assert.IsFalse(hasListener);
    }

    /// <summary>
    /// Tests whether the <c>EventManager</c> has no listener if the list
    /// of registered event listeners is emptied.
    /// </summary>
    [Test]
    public void HasNoListenerWhenRemovedAll()
    {
        EventManager.AddListener<GameEvent>(eventHandler.HandleGameEvent);
        EventManager.RemoveAll();
        bool hasListener = EventManager.HasListener<GameEvent>(eventHandler.HandleGameEvent);

        // Assert that EventManager has no listener if list of registered event listeners is emptied.
        Assert.IsFalse(hasListener);
    }

    /// <summary>
    /// Tests whether the <c>HandleGameEvent()</c> is called after firing an event.
    /// </summary>
    [Test]
    public void TriggeringEventShouldCallEventHandler()
    {
        EventManager.AddListener<GameEvent>(eventHandler.HandleGameEvent);
        GameEvent gameEvent = new GameEvent();
        EventManager.TriggerEvent(gameEvent);

        // Assert that HandleGameEvent() is called after firing an event.
        eventHandler.Received().HandleGameEvent(gameEvent);
    }

    /// <summary>
    /// Tests whether the <c>HandleGameEvent()</c> is called only once after firing
    /// the second event. The second event should not call the <c>HandleGameEvent()</c>.
    /// </summary>
    [Test]
    public void OneTimeRegisteringListenerShouldBeCalledOnlyOnce()
    {
        EventManager.AddListenerOnce<GameEvent>(eventHandler.HandleGameEvent);
        GameEvent gameEvent = new GameEvent();
        EventManager.TriggerEvent(gameEvent);
        EventManager.TriggerEvent(gameEvent);

        // Since the listener is added using AddListenerOnce(), assert that HandleGameEvent()
        // is called only once after firing the second event. The second event should not call
        // the HandleGameEvent().
        eventHandler.Received(1).HandleGameEvent(gameEvent);
    }
}
