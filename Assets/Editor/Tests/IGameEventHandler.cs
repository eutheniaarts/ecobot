﻿public interface IGameEventHandler
{
    void HandleGameEvent(GameEvent gameEvent);
}